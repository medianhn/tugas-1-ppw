# from django.test import TestCase
# from django.test import Client
# from django.urls import resolve
# from django.http import HttpRequest
# from .views import index
# from .models import Friends

# class AddfriendUnitTest(TestCase):
# 	def test_add_friend_url_is_exist(self):
# 		response = Client().get('/friends/')
# 		self.assertEqual(response.status_code, 200)

# 	def test_addfriend_using_index_func(self):
# 		found = resolve('/friends/')
# 		self.assertEqual(found.func, index)

# 	def test_addfriend_using_friends_list_template(self):
# 		response = Client().get('/friends/')
# 		self.assertTemplateUsed(response, 'friends_list.html')
	
# 	def test_model_can_add_new_friend(self):
# 		# Creating a new activity
# 		new_friend = Friend.objects.create(Name='Naufal Pangestu Zandroto', URL='instagram.com/7.79.9.13')

# 		# Retrieving all available activity
# 		counting_all_available_friend = Friends.objects.all().count()
# 		self.assertEqual(counting_all_available_friend, 1)

# 	def test_addfriend_request(self):
# 		response = self.client.post('/friends/add_friend/', data={'Name': 'Nobody', 'URL' : ''})
# 		counting_all_available_activity = Friends.objects.all().count()
# 		self.assertEqual(counting_all_available_activity, 1)
		
# 		self.assertEqual(response.status_code, 302)
# 		self.assertEqual(response['location'], '/friends/')
		
# 		new_response = self.client.get('/friends/')
# 		html_response = new_response.content.decode('utf8')
# 		self.assertIn('Nobody', html_response)
# 		
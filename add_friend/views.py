# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render, redirect
from .models import Friends
from datetime import datetime, date
from app_profile_page.active_profile import get_active_profile
import pytz
import json

# Create your views here.
friends_dict = {}

def index(request):
	friends_dict = Friends.objects.all().values()
	return render(request, 'friends_list.html', {'friends_dict' : friends_dict})
	
def add_friend(request):
	if request.method == 'POST':
		Friends.objects.create(name=request.POST['name'], URL=request.POST['URL'], date = datetime.now())
		add_friends()
		return redirect('/friends/')
	
def convert_queryset_into_json(queryset):
	ret_val = []
	for data in queryset:
		ret_val.append(data)
	return ret_val

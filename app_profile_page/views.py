from django.shortcuts import render
from datetime import datetime, date
from .profile import get_active_profile

exp = ['QA', 'Tester', 'Project Manageer']
def index(request):

    profile = get_active_profile()
    response = {'name': profile.name, 'gender': profile.gender,'birth': profile.birth_date,
                'email': profile.email,'picture': profile.picture_url,'expertise': exp,
                'description': profile.description}
    return render(request, 'profile_page.html', response)

from django.db import models

class Profile(models.Model):
    name = models.CharField(max_length=120)
    gender = models.CharField(max_length=10)
    birth_date = models.DateField()
    email = models.EmailField()
    picture_url = models.URLField()
    expertise =  models.TextField()
    description = models.CharField(max_length=100)
    

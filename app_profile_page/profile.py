from datetime import date
from .models import Profile


NAME = 'Ester Theresia'
EMAIL = 'ester@sihotang.com'
PICTURE_URL = 'https://c2.staticflickr.com/4/3006/2649581844_984d2a084a_b.jpg'
GENDER = 'Female'
DESCRIPTION = 'Antique expert. Experience as marketer for 10 years'
BIRTH = date(1998, 6, 28)
EXPERTISE = 'Marketing', 'Collector', 'Programmer'

def get_active_profile():
    if not Profile.objects.all():
        create_new_profile()
    return Profile.objects.all()[len(Profile.objects.all()) - 1]

def create_new_profile():
    Profile.objects.create(name=NAME, email=EMAIL, picture_url=PICTURE_URL, gender=GENDER,
                           description=DESCRIPTION, birth_date=BIRTH, expertise=EXPERTISE)

from django import forms

class Status_Form(forms.Form):
    status_attrs = {
        'placeholder':"Apa yang kamu pikirkan?",
        'class' : 'form-control',
        'type': 'text',
        'rows': 8,
        'style' : 'resize:none'
    }

    status = forms.CharField(label='', required=True, widget=forms.Textarea(attrs=status_attrs))

class Comment_Form(forms.Form):
    nama_attrs = {
        'placeholder':"Berikan nama Anda :",
        'type': 'text',
        'class' : 'form-control',
        'rows': 1,
        'style' : 'resize:none'
    }
    comment_attrs = {
        'placeholder':"Berikan komentar Anda disini",
        'class' : 'form-control',
        'type': 'text',
        'rows': 4,
        'style' : 'resize:none'
    }
    comment = forms.CharField(label='', required=True, widget=forms.Textarea(attrs=comment_attrs))
    nama = forms.CharField(label='', required=True, widget=forms.Textarea(attrs=nama_attrs))
    
    
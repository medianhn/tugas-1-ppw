from django.db import models

class Status(models.Model):
    status = models.TextField()
    created_date = models.DateTimeField(auto_now_add=True)

class Comments(models.Model):
    nama = models.TextField()
    comment = models.TextField()
    post = models.ForeignKey(Status, on_delete=models.CASCADE)
    comments_created_date = models.DateTimeField(auto_now_add=True)

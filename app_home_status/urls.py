from django.conf.urls import url
from .views import index, tambah_status, comment, tambah_comment

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^tambah_status', tambah_status, name='tambah_status'),
    url(r'^comment/(?P<id>\w{0,50})/$', comment, name='comment'),
    url(r'^tambah_comment/(?P<id>\w{0,50})/$', tambah_comment, name='tambah_comment'),
]
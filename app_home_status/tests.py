from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .models import Status, Comments
from .forms import Status_Form, Comment_Form
from .views import comment
from django.http import HttpRequest


# Create your tests here.
class StatusPageUnitTest(TestCase):
    # def test_status_home_is_exist():
    #     response = Client().get('/home-status/')
    #     self.assertEqual(response.status_code, 200)

    def test_comment_link_using_comment_(self):
        status1 = Status(status ='tess yaa',pk=1)
        found = resolve('/home-status/comment/1/')
        self.assertEqual(found.func, comment)

    def test_comment_form(self):
        form = Comment_Form()
        self.assertIn('id="id_nama"', form.as_p())
        self.assertIn('id="id_comment', form.as_p())
        self.assertIn('class="form-control', form.as_p())
        self.assertIn('class="form-control', form.as_p())

    def test_comment_page_is_exist(self):
        status1 = Status(status ='cobaaa',pk=1)
        response = Client().get('/home-status/comment/1')
        self.assertEqual(response.status_code, 301,200)

    def test_root_url_using_index_from_home_status(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 301)
        self.assertRedirects(response,'/home-status/',301,200)

    def test_model_can_create_new_status(self):
        new_activity = Status.objects.create(status ='coba ya guys')
        counting_all_available_Status= Status.objects.all().count()
        self.assertEqual(counting_all_available_Status,1)

    def test_model_create_status_wrongly(self):
        response = Client().post('/home-status/tambah_status', {'status' : ''})
        self.assertEqual(response.status_code, 302)

    def test_status_showing_all_status(self):
        status = 'semangat cuy'
        new_status = {'status' : status}
        post_new_status = Client().post('/home-status/tambah_status', new_status)
        response = Client().get('/home-status/')
        html_response = response.content.decode('utf8')
        self.assertIn(status, html_response)


        
        


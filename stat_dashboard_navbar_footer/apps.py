from django.apps import AppConfig


class StatDashboardNavbarFooterConfig(AppConfig):
    name = 'stat_dashboard_navbar_footer'
